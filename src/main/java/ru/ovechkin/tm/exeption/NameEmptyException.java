package ru.ovechkin.tm.exeption;

public class NameEmptyException extends RuntimeException {

    public NameEmptyException() {
        super("Error! Name is empty...");
    }

}