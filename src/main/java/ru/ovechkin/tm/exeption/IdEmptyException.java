package ru.ovechkin.tm.exeption;

public class IdEmptyException extends RuntimeException {

    public IdEmptyException() {
        super("Error! ID is empty...");
    }

}