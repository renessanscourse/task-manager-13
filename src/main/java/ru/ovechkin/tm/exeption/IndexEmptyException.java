package ru.ovechkin.tm.exeption;

public class IndexEmptyException extends RuntimeException {

    public IndexEmptyException() {
        super("Error! Index is empty...");
    }

}