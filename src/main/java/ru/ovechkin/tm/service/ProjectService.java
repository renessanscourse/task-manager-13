package ru.ovechkin.tm.service;

import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.exeption.IdEmptyException;
import ru.ovechkin.tm.exeption.IndexEmptyException;
import ru.ovechkin.tm.exeption.NameEmptyException;
import ru.ovechkin.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) {
        if (name.isEmpty() || name == null) throw new NameEmptyException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        final Project project = new Project();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void removeProject(final Project project) {
        projectRepository.remove(project);
    }

    @Override
    public void removeAllProjects() {
        projectRepository.clear();
    }

    @Override
    public Project findProjectById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.findById(id);
    }

    @Override
    public Project findProjectByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexEmptyException();
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project findProjectByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return projectRepository.findByName(name);
    }

    @Override
    public Project updateProjectById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findProjectById(id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findProjectByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeProjectById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeProjectByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexEmptyException();
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project removeProjectByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return projectRepository.removeByName(name);
    }

}