package ru.ovechkin.tm.service;

import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.exeption.IdEmptyException;
import ru.ovechkin.tm.exeption.IndexEmptyException;
import ru.ovechkin.tm.exeption.NameEmptyException;
import ru.ovechkin.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void removeTask(final Task task) {
        taskRepository.remove(task);
    }

    @Override
    public void removeAllTasks() {
        taskRepository.clear();
    }

    @Override
    public Task findTaskById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findById(id);
    }

    @Override
    public Task findTaskByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexEmptyException();
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task findTaskByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return taskRepository.findByName(name);
    }

    @Override
    public Task updateTaskById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findTaskById(id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findTaskByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeTaskById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeTaskByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexEmptyException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeTaskByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return taskRepository.removeByName(name);
    }

}